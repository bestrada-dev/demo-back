package com.demo01Back.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demo01Back.model.ParParametro;

import java.util.List;


@Repository

public interface ParParametroDao{
    List<ParParametro> findAll();
}
