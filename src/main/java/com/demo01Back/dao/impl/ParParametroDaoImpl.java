	
	package com.demo01Back.dao.impl;

	import java.util.List;
	import javax.persistence.EntityManager;
	import javax.persistence.PersistenceContext;
	import javax.transaction.Transactional;
	import org.springframework.stereotype.Repository;

import com.demo01Back.dao.ParParametroDao;
import com.demo01Back.model.ParParametro;
	@Transactional
	@Repository
	public class ParParametroDaoImpl implements ParParametroDao {
		@PersistenceContext	
		private EntityManager entityManager;	
		

		@SuppressWarnings("unchecked")
		@Override
		public List<ParParametro> findAll() {
			String hql = "FROM ParParametro as p ORDER BY p.parCodigo DESC";
			return (List<ParParametro>) entityManager.createQuery(hql).getResultList();
		}


	
		
		
	}