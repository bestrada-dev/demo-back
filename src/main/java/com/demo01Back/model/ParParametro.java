
package com.demo01Back.model;

import com.fasterxml.jackson.annotation.JsonIgnore;


import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "PAR_PARAMETRO")
public class ParParametro {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String parCodigo;
	private String parNombre;
	private String parTipo;
	
	
	
	public String getParCodigo() {
		return parCodigo;
	}
	public void setParCodigo(String parCodigo) {
		this.parCodigo = parCodigo;
	}
	public String getParNombre() {
		return parNombre;
	}
	public void setParNombre(String parNombre) {
		this.parNombre = parNombre;
	}
	public String getParTipo() {
		return parTipo;
	}
	public void setParTipo(String parTipo) {
		this.parTipo = parTipo;
	}
	

}
