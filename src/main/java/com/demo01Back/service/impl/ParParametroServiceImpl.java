	
	package com.demo01Back.service.impl;

	import java.util.List;
	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.stereotype.Service;

import com.demo01Back.dao.*;
import com.demo01Back.model.*;
import com.demo01Back.service.*;

	@Service
	public class ParParametroServiceImpl implements ParParametroService{

		@Autowired
		private ParParametroDao parParametroDao;
		
		@Override
		public List<ParParametro> findAll() {
			// TODO Auto-generated method stub
			return parParametroDao.findAll();
		}

	}
