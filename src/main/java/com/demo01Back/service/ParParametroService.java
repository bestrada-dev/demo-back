package com.demo01Back.service;

import java.util.List;

import com.demo01Back.model.ParParametro;

public interface ParParametroService {
    List<ParParametro> findAll();
}
